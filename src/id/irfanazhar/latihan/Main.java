package id.irfanazhar.latihan;

import java.util.Scanner;

public class Main {

    static Scanner input = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("----------------------------------------");
        System.out.println("Kalkulator Penghitung Luas dan Volume");
        System.out.println("----------------------------------------");
        mainMenu();
    }

    public static void mainMenu() {
        System.out.println("Menu");
        System.out.println("1. Hitung Luas Bidang");
        System.out.println("2. Hitung Volume Bidang");
        System.out.println("0. Tutup Aplikasi");
        System.out.println("----------------------------------------");
        byte calculationType = input.nextByte();
        switch (calculationType) {
            case 1:
                new Main().calculateArea();
                break;
            case 2:
                new Main().calculateVolume();
                break;
            case 0:
                System.out.println("Terima Kasih");
                break;
            default:
                mainMenu();
                break;
        }
    }
    public void calculateArea() {
        System.out.println("----------------------------------------");
        System.out.println("Pilih Bidang yang akan dihitung");
        System.out.println("----------------------------------------");
        System.out.println("1. Persegi");
        System.out.println("2. Lingkaran");
        System.out.println("3. Segitiga");
        System.out.println("4. Persegi Panjang");
        System.out.println("0. Kembali ke menu sebelumnya");
        System.out.println("----------------------------------------");
        byte calculationType = input.nextByte();
        switch (calculationType) {
            case 1:
                calculateSquareArea();
                break;
            case 2:
                calculateCircleArea();
                break;
            case 3:
                calculateTriangleArea();
                break;
            case 4:
                calculateRectangleArea();
                break;
            case 0:
                mainMenu();
                break;
            default:
                calculateArea();
                break;
        }
    }

    public void calculateVolume() {
        System.out.println("----------------------------------------");
        System.out.println("hitung volume bidang");
        System.out.println("----------------------------------------");
        System.out.println("1. Kubus");
        System.out.println("2. Balok");
        System.out.println("3. Tabung");
        System.out.println("0. Kembali ke menu sebelumnya");
        System.out.println("----------------------------------------");
        byte calculationType = input.nextByte();
        switch (calculationType) {
            case 1:
                calculateCubeVolume();
                break;
            case 2:
                calculateCuboidVolume();
                break;
            case 3:
                calculateCylinderVolume();
                break;
            case 0:
                mainMenu();
                break;
            default:
                calculateVolume();
                break;
        }
    }

    public void calculateSquareArea() {
        System.out.println("----------------------------------------");
        System.out.println("Anda memilih persegi");
        System.out.println("----------------------------------------");
        System.out.print("Masukkan sisi: ");
        float sisi = input.nextFloat();
        System.out.println("\nprocessing... \n");
        float luas = sisi * sisi;
        System.out.println("Luas dari persegi adalah " + luas);
        System.out.println("----------------------------------------");
        backToMainMenu();
    }

    public void calculateCircleArea() {
        System.out.println("----------------------------------------");
        System.out.println("Anda memilih lingkaran");
        System.out.println("----------------------------------------");
        System.out.print("Masukkan jari-jari: ");
        float jari = input.nextFloat();
        System.out.println("\nprocessing...\n");
        float luas = (float) (Math.PI * jari * jari);
        System.out.println("Luas dari lingkaran adalah " + luas);
        System.out.println("----------------------------------------");
        backToMainMenu();
    }

    public void calculateTriangleArea() {
        System.out.println("----------------------------------------");
        System.out.println("Anda memilih segitiga");
        System.out.println("----------------------------------------");
        System.out.print("Masukkan alas: ");
        float alas = input.nextFloat();
        System.out.print("Masukkan tinggi:");
        float tinggi = input.nextFloat();
        System.out.println("\nprocessing... \n");
        float luas = (alas * tinggi) / 2;
        System.out.println("Luas dari segitiga adalah " + luas);
        System.out.println("----------------------------------------");
        backToMainMenu();
    }
    public void calculateRectangleArea() {
        System.out.println("----------------------------------------");
        System.out.println("Anda memilih persegi panjang");
        System.out.println("----------------------------------------");
        System.out.print("Masukkan panjang: ");
        float panjang = input.nextFloat();
        System.out.print("Masukkan lebar: ");
        float lebar = input.nextFloat();
        System.out.println("\nprocessing...\n");
        float luas = panjang * lebar;
        System.out.println("Luas dari persegi panjang adalah " + luas);
        System.out.println("----------------------------------------");
        backToMainMenu();
    }

    public void calculateCubeVolume() {
        System.out.println("----------------------------------------");
        System.out.println("Anda memilih Kubus");
        System.out.println("----------------------------------------");
        System.out.print("Masukkan sisi: ");
        float sisi = input.nextFloat();
        System.out.println("\nprocessing...\n");
        float volume = sisi * sisi * sisi;
        System.out.println("Volume dari kubus adalah " + volume);
        System.out.println("----------------------------------------");
        backToMainMenu();
    }

    public void calculateCuboidVolume() {
        System.out.println("----------------------------------------");
        System.out.println("Anda memilih balok");
        System.out.println("----------------------------------------");
        System.out.print("Masukkan panjang: ");
        float panjang = input.nextFloat();
        System.out.print("Masukkan lebar: ");
        float lebar = input.nextFloat();
        System.out.print("Masukkan tinggi: ");
        float tinggi = input.nextFloat();
        System.out.println("\nprocessing...\n");
        float volume = panjang * lebar * tinggi;
        System.out.println("Volume dari balok adalah " + volume);
        System.out.println("----------------------------------------");
        backToMainMenu();
    }
    public void calculateCylinderVolume() {
        System.out.println("----------------------------------------");
        System.out.println("Anda memilih tabung");
        System.out.println("----------------------------------------");
        System.out.print("Masukkan jari-jari: ");
        float jari = input.nextFloat();
        System.out.print("Masukkan tinggi: ");
        float tinggi = input.nextFloat();
        System.out.println("\nprocessing...\n");
        float volume = (float) (Math.PI * jari * jari * tinggi);
        System.out.println("Volume dari tabung adalah " + volume);
        System.out.println("----------------------------------------");
        backToMainMenu();
    }

    public void backToMainMenu(){
        System.out.println("tekan apa saja untuk kembali ke menu utama");
        input.next();
        mainMenu();
    }
}