package id.irfanazhar.latihan;

public class Loop {
    public static void main(String args[]) {
        int[] primeNumbers = {1, 2, 3, 5, 7, 11};

        int result = sum(10);
        System.out.println(result);

        for(int i = 1; i <= 6; i++) {
            if (i <= 3) {
                System.out.print(2 * i + " ");
            } else {
                System.out.print(pow(2, i - 1) + " ");
            }
        }
        System.out.println();
        for(int i = 0; i < 5; i++) {
            System.out.print(pow(2, primeNumbers[i]) + " ");
        }
        System.out.println();

        int[] numArray = {6, 6, 5, 9, 2};
        for (int num : sortNumber(numArray)) {
            System.out.print(num + " ");
        }
        System.out.println();
        char[] charArray = {'M', 'A', 'K', 'A', 'N', 'A', 'S', 'I'};
        int[] charArrayAscii = new int[charArray.length];
        for (int i = 0; i < charArray.length; i++) {
            charArrayAscii[i] = (int) charArray[i];
        }
        for (int karakter: sortNumber(charArrayAscii)) {
            System.out.print((char)karakter + " ");
        }
    }

    public static int pow(int x, int y) {
        if (y <= 0) {
            return 1;
        }
        return x * pow(x, y - 1);
    }

    public static int[] sortNumber(int[] numbers) {
        int[] result = new int[numbers.length];
        for (int i = 0; i < numbers.length; i++) {
            int temp = numbers[i];
            int tempIndex = i;
            for (int j = i + 1; j < numbers.length; j++) {
                if (temp > numbers[j]) {
                    temp = numbers[j];
                    tempIndex = j;
                }
            }
            result[i] = temp;
            numbers[tempIndex] = numbers[i];
        }
        return result;
    }

    public int fib(int n) {
        if (n <= 1) {
            return n;
        }
        return fib(n - 1) + fib(n - 2);
    }

    public static int sum(int k) {
        if (k <= 0) {
            return 0;
        }
        return k + sum(k - 1);
    }
}
