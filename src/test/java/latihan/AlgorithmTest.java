package latihan;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AlgorithmTest {
//
//    @Test
//    @DisplayName("Bisa is an anagram of bias")
//    void isAnagram() {
//        boolean result = Algorithm.isAnagram("Bisa", "bias");
//        assertTrue(result);
//    }
//
//    @Test
//    @DisplayName("Katak is not an anagram of Kodok")
//    void isNotAnagram() {
//        boolean result = Algorithm.isAnagram("Katak", "Kodok");
//        assertFalse(result);
//    }

    @Test
    @DisplayName("tes algoritma kodok")
    void testFrog() {
        int[] arr = {1, 2, 3, 5, 4, 3, 2};
        int result = Algorithm.solution(5, arr);
        assertEquals(4, result);
    }

    @Test
    @DisplayName("tes algoritma kodok 2")
    void testFrog2() {
        int[] arr = {5, 6, 1, 2, 4, 3, 2, 4};
        int result = Algorithm.solution(6, arr);
        assertEquals(5, result);
    }

    @Test
    @DisplayName("tes algoritma kodok bolong")
    void testFrog3() {
        int[] arr = {5, 6, 1, 2, 4, 7, 2, 4};
        int result = Algorithm.solution(7, arr);
        assertEquals(-1, result);
    }
}