package challenge3;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

class MarksTest {

    static Marks marks1;
    static Marks marks2;

    @BeforeEach
    void setUp() {
        ArrayList<Integer> markList1 = new ArrayList<>(Arrays.asList(7, 8, 7, 8, 9, 5, 10, 9, 9));
        ArrayList<Integer> markList2 = new ArrayList<>(Arrays.asList(5, 10, 8 , 7, 5, 6, 7, 8, 8, 9, 8, 8));
        marks1 = new Marks(markList1);
        marks2 = new Marks(markList2);
    }

    @Test
    @DisplayName("Test for getMarks")
    void getMarks() {
        ArrayList<Integer> expectedResult1 = new ArrayList<>(Arrays.asList(7, 8, 7, 8, 9, 5, 10, 9, 9));
        assertEquals(expectedResult1, marks1.getMarks());
        assertEquals(7, marks1.getMarks(0));
    }

    @Test
    @DisplayName("Test for getSortedMarks")
    void getSortedMarks() {
        ArrayList<Integer> expectedResult = new ArrayList<>(Arrays.asList(7, 8, 7, 8, 9, 5, 10, 9, 9));
        Collections.sort(expectedResult);
        assertEquals(expectedResult, marks1.getSortedMarks());
        assertEquals(5, marks1.getSortedMarks(0));
    }


    @Test
    @DisplayName("Test for setMarks")
    void setMarks() {
        ArrayList<Integer> markList = new ArrayList<>(Arrays.asList(10, 9, 10, 8, 7, 8));
        marks1.setMarks(markList);
        ArrayList<Integer> expectedResult = new ArrayList<>(Arrays.asList(10, 9, 10, 8, 7, 8));
        assertEquals(expectedResult, marks1.getMarks());
        ArrayList<Integer> sortedMarks = new ArrayList<>(expectedResult);
        Collections.sort(sortedMarks);
        assertEquals(sortedMarks, marks1.getSortedMarks());
    }

    @Test
    @DisplayName("Test for getMean")
    void getMean() {
        assertEquals(8.0, marks1.getMean());
        assertEquals(7.416666507720947, marks2.getMean());
    }

    @Test
    @DisplayName("Test for getMedian")
    void getMedian() {
        assertEquals(8.0, marks1.getMedian());
        assertEquals(8.0, marks2.getMedian());
    }

    @Test
    @DisplayName("Test for getMode")
    void getMode() {
        assertEquals(9, marks1.getMode());
        assertEquals(8, marks2.getMode());
    }
    @Test
    @DisplayName("Test for getFrequency")
    void getFrequency() {
        assertEquals(1, marks1.getFrequency(10));
        assertEquals(3, marks1.getFrequency(9, 9));
        assertEquals(2, marks1.getFrequency(8));
        assertEquals(4, marks1.getFrequency(7, 8));
        assertEquals(1, marks1.getFrequency(5));
        assertEquals(2, marks2.getFrequency(5));
        assertEquals(1, marks2.getFrequency(10));
        assertEquals(5, marks2.getFrequency(8));
    }
}