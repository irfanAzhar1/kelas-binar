package latihan;

import java.util.ArrayList;
import java.util.HashSet;

public class Algorithm {
    public static boolean isAnagram(String str1, String str2) {
        if (str1.length() != str2.length()) return false;

        ArrayList<Character> list1 = new ArrayList<>();

        for (char c : str1.toLowerCase().toCharArray()) {
            list1.add(c);
        }

        for (char c : str2.toLowerCase().toCharArray()) {
            int index = list1.indexOf(c);
            if (index == -1) return false;
            list1.remove(index);
        }
        return true;
    }
    
    public static int solution(int X, int[] A) {
        ArrayList<Integer> leafPosition = new ArrayList<>();
        for (int n: A) {
            leafPosition.add(n);
        }
        int max = 0;
        for (int i = 1; i <= X; i++) {
            int index = leafPosition.indexOf(i);
            if (index == -1) return -1;
            if (index > max) max = index;
        }

        return max;
    }

}
