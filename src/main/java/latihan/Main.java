package latihan;

import java.util.ArrayList;

public class Main {
    public static void main (String[] args) {
        ArrayList<String> words = new ArrayList<>();
        words.add("abc");
        words.add("def");
        words.add("ghij");

        words.removeIf(word -> word.length() > 3);
        words.forEach(word -> System.out.println(word));

    }
}
