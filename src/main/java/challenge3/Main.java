package challenge3;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws IOException {
        Marks marks = null;

        String path = "src/main/java/challenge3/data/data_sekolah.csv";
        try {
            marks = new Marks(openFile(path));
        } catch (IOException e) {
            System.out.println("File tidak ditemukan");
            e.printStackTrace();
        }

        mainMenu(marks);

    }

    public static void mainMenu(Marks school) throws IOException {
        Scanner scanner = new Scanner(System.in);

        System.out.println("----------------------------------------------------------------");
        System.out.println("Aplikasi Pengolah Nilai Sekolah");
        System.out.println("----------------------------------------------------------------");
        System.out.println("pilih menu: ");
        System.out.println("1. Generate txt untuk menampilkan modus");
        System.out.println("2. Generate txt untuk menampilkan nilai rata-rata, dan median");
        System.out.println("3. generate kedua file");
        System.out.println("0. exit");

        byte input = scanner.nextByte();

        switch (input) {
            case 1:
                System.out.println("----------------------------------------------------------------");
                System.out.println("Aplikasi Pengolah Nilai Sekolah");
                System.out.println("----------------------------------------------------------------");
                generateMode(school);
                backToMainMenu(school);
                break;
            case 2:
                System.out.println("----------------------------------------------------------------");
                System.out.println("Aplikasi Pengolah Nilai Sekolah");
                System.out.println("----------------------------------------------------------------");
                generateMeanMedian(school);
                backToMainMenu(school);
                break;
            case 3:
                System.out.println("----------------------------------------------------------------");
                System.out.println("Aplikasi Pengolah Nilai Sekolah");
                System.out.println("----------------------------------------------------------------");
                generateMode(school);
                generateMeanMedian(school);
                backToMainMenu(school);
                break;
            case 0:
                System.exit(0);
                break;
            default:
                System.out.println("input tidak valid");
                mainMenu(school);
        }

    }

    public static ArrayList<Integer> openFile(String path) throws IOException {
        File file = new File(path);

        System.out.println("Letakan file csv dengan nama file data_sekolah.csv\ndi direktori berikut " + file.getAbsolutePath());

        BufferedReader dataNilai = new BufferedReader(new FileReader(file));

        ArrayList<Integer> marks = new ArrayList<>();

        String line;
        while ( (line = dataNilai.readLine()) != null) {
            String[] data = line.split(";");
            for (int i = 1; i < data.length; i++) {
                marks.add(Integer.parseInt(data[i]));
            }

        }

        return marks;
    }

    public static void generateMeanMedian(Marks school) throws IOException {
        String path  = "src/main/java/challenge3/data/data_mean_median.txt";
        File file = new File(path);
        BufferedWriter writer = new BufferedWriter(new FileWriter(file));

        writer.write("Berikut Hasil Pengolahan Nilai: \n");
        writer.newLine();
        writer.write("Rata-rata: " + school.getMean());
        writer.newLine();
        writer.write("Median: " + school.getMedian());
        writer.newLine();
        writer.write("Modus: " + school.getMode());
        writer.newLine();

        writer.close();

        System.out.println("file telah di generate di " + file.getAbsolutePath());
    }

    public static void generateMode(Marks school) throws IOException {
        String path = "src/main/java/challenge3/data/data_modus.txt";
        File file = new File(path);
        BufferedWriter writer = new BufferedWriter(new FileWriter(file));

        writer.write("Berikut Hasil Pengolahan Nilai: \n");
        writer.newLine();
        writer.write("Nilai \t \t | \t \t Frekuensi");
        writer.newLine();
        writer.write("kurang dari 6 \t \t | \t \t " + school.getFrequency(0, 5));
        writer.newLine();
        writer.write("6 \t \t | \t \t " + school.getFrequency(6));
        writer.newLine();
        writer.write("7 \t \t | \t \t " + school.getFrequency(7));
        writer.newLine();
        writer.write("8 \t \t | \t \t " + school.getFrequency(8));
        writer.newLine();
        writer.write("9 \t \t | \t \t " + school.getFrequency(9));
        writer.newLine();
        writer.write("10 \t \t | \t \t " + school.getFrequency(10));
        writer.close();

        System.out.println("file telah di generate di " + file.getAbsolutePath());
    }

    public static void backToMainMenu(Marks marks) throws IOException {
        Scanner scanner = new Scanner(System.in);

        System.out.println();
        System.out.println("0. exit");
        System.out.println("1. Kembali ke menu utama");

        byte input = scanner.nextByte();
        switch (input) {
            case 0:
                System.exit(0);
                break;
            case 1:
                mainMenu(marks);
                break;
            default:
                System.out.println("input tidak valid");
                backToMainMenu(marks);
        }
    }
}

