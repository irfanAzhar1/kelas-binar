package challenge3;

import java.util.ArrayList;
import java.util.Collections;
import java.util.TreeMap;
import java.util.stream.Collectors;

public class Marks {
    private ArrayList<Integer> marks;
    private ArrayList<Integer> sortedMarks;

    public Marks(ArrayList<Integer> marks){
        this.marks = marks;
        sortedMarks = new ArrayList<>(marks);
        Collections.sort(sortedMarks);
    }

    public ArrayList<Integer> getMarks() {
        return this.marks;
    }

    public int getMarks(int i) {
        return this.marks.get(i);
    }

    public ArrayList<Integer> getSortedMarks() {
        return this.sortedMarks;
    }

    public int getSortedMarks(int i) {
        return this.sortedMarks.get(i);
    }

    public void setMarks(ArrayList<Integer> marks) {
        this.marks = marks;
        sortedMarks = new ArrayList<>(marks);
        Collections.sort(sortedMarks);
    }

    public float getMean(){
        return (float) marks.stream()
                .mapToDouble(Integer::doubleValue)
                .average()
                .orElse(Double.NaN);
    }

    public float getMedian() {
        return sortedMarks.size() % 2 == 0 ?
                ((float) sortedMarks.get(sortedMarks.size() / 2) + (float) sortedMarks.get(sortedMarks.size() / 2 - 1)) / 2 :
                sortedMarks.get(sortedMarks.size() / 2);
    }

    public int getMode(){
        return marks.stream()
                .collect(Collectors.groupingBy(i -> i, TreeMap::new, Collectors.counting()))
                .entrySet().stream().sorted((a, b) -> {
                    if (!a.getValue().equals(b.getValue()))
                        return b.getValue().compareTo(a.getValue());
                    return a.getKey().compareTo(b.getKey());
                }).findFirst().get().getKey();
    }

    public int getFrequency(int min, int max){
        return marks
                .stream()
                .reduce(0, (sum, mark) -> sum + (mark >= min && mark <= max ? 1 : 0));
    }

    public int getFrequency(int num) {
        return getFrequency(num, num);
    }
}

